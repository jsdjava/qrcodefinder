# QRCodeFinder
## Demo

![QR Code Finder](pics/qrcodefinder.gif)

## Description
QR Code Finder is a project I wrote for a lab in a Computer Vision class. It finds QR codes in static images or a camera
stream.

Major features completed are:
* Camera stream -> image collector
* Standalone image processor
* Uses a bunch of different image filtering techniques (gaussians, derivatives, etc) to find find possible locations for a QR code
  and then pick the most likely ones 
* Draws where the QRCode is (including corners and the square outline)

## Running Locally
In vscode, should be able to just run because I included the libs as jars. Any images you want to process (png or jpeg)
can be placed in the main/top folder.

## Attribution
Advice/Help from Dr. Yoder at MSOE
