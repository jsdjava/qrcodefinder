package qrcodereader;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import visionlibrary.QRAlgoStep;
import visionlibrary.QRCodeData;
import visionlibrary.QRPolygon;

/**
 * The QRAlgoAreaFilter takes a list of 9 polygons, three at each corner of the
 * QR Code, and determines where approximately the fourth corner should be using
 * a lot of geometry and helper slope and angle methods from the QRPolygon class.
 *
 * @author daviesj
 */
public class QRAlgoAreaFilter extends QRAlgoStep {

    private QRAlgoStep polyStep;

    /**
     * The default constructor, which requires that the previous step was one that
     * produced polygons.
     *
     * @param polyStep The previous algorithm step that hopefully filtered the polygons down to just the QRCode corners.
     */
    public QRAlgoAreaFilter(QRAlgoStep polyStep) {
        this.polyStep = polyStep;
    }

    /**
     * This iterates through the list of polygons and uses the largest three corner
     * polygons to figure out where a triangle covering about 1/2 the area of the QR Code is.
     * It then attempts to calculate where the fourth corner is using the getQRArea helper method.
     *
     * @return
     */
    @Override
    public QRCodeData completeStep() {
        QRCodePolygons areaData = new QRCodePolygons();
        if (polyStep instanceof QRAlgoPolygonFilter) {
            QRCodePolygons qrPolygonData = (QRCodePolygons) polyStep.getData(); //Currently ,0th is smallest contained,1st is middle contained and 2nd is the container
            for (List<QRPolygon> curPolyList : qrPolygonData.getPolygons()) {
                List<QRPolygon> polyAreaList = new ArrayList();
                if (curPolyList.size() == 9) {
                    for (int i = 2; i < 3; i++) {
                        polyAreaList.add(getQRArea(curPolyList.get(i), curPolyList.get(i + 3), curPolyList.get(i + 6)));
                        polyAreaList.add(new QRPolygon(new Point[]{curPolyList.get(i).getQuadriCenterPoint(), curPolyList.get(i + 3).getQuadriCenterPoint(), curPolyList.get(i + 6).getQuadriCenterPoint()}));
                    }
                }
                areaData.addPolygons(polyAreaList);
            }
        }
        result = areaData;
        return areaData;
    }

    /**
     * This determines where the fourth corner is using the original three corner.
     * It first attempts to determine which corner is the center one by comparing each of its
     * angles to the angles inside the triangle formed by the centerpoints by all the corners.
     * The one that seems to have the highest minimum angle difference is probably the one that
     * is the corner. I also had to add in a clause that checks to see if the difference is greater than
     * 150 degrees, which means that the angle is probably in the wrong direction, and I should only
     * count it as 180-that 150. This is the geometry part that I'm not quite sure I did right, although it
     * works for a variety of rotations, translations, and scalings. Once it has the corner one,
     * it uses the opposing slopes in the other two polygons to calculate an intersection point,
     * link these points together into a 4 sided polygon, and finally grows it in all directions
     * by the original length of a corner divided by two to move it from the centerpoints of the
     * corner polygons to the outermost regions.
     *
     * @param rect1 One of the qr corner polygons
     * @param rect2 Another qr corner polygon
     * @param rect3 The final qr corner polygon
     * @return A 4 sided region that approximately encompasses the entire QR Code.
     */
    private QRPolygon getQRArea(QRPolygon rect1, QRPolygon rect2, QRPolygon rect3) {
        QRPolygon qrTriangle = new QRPolygon(new Point[]{rect1.getQuadriCenterPoint(), rect2.getQuadriCenterPoint(), rect3.getQuadriCenterPoint()});
        double[] triAngles = qrTriangle.getTriangleAngles();
        QRPolygon[] qrAreas = new QRPolygon[]{rect1, rect2, rect3};
        double[] minDiffs = new double[3];
        double[] minSlopes = new double[3];
        double[] answer0 = qrAreas[0].calculateMinDiff(triAngles[1]);
        minDiffs[0] = answer0[0];
        minSlopes[0] = answer0[1];
        double[] answer1 = qrAreas[1].calculateMinDiff(triAngles[2]);
        minDiffs[1] = answer1[0];
        minSlopes[1] = answer1[1];
        double[] answer2 = qrAreas[2].calculateMinDiff(triAngles[0]);
        minDiffs[2] = answer2[0];
        minSlopes[2] = answer2[1];

        Point intersectPoint = new Point(0, 0);
        QRPolygon qrArea = new QRPolygon(new Point[]{});
        if (minDiffs[0] > minDiffs[2] && minDiffs[0] > minDiffs[1]) {
            //Fringe ones are 2nd and 1st
            intersectPoint = qrAreas[0].getLineIntersection(minSlopes[2], qrAreas[2].getQuadriCenterPoint(), minSlopes[1], qrAreas[1].getQuadriCenterPoint());
            qrArea = new QRPolygon(new Point[]{qrAreas[0].getQuadriCenterPoint(), qrAreas[1].getQuadriCenterPoint(), intersectPoint, qrAreas[2].getQuadriCenterPoint()});
            //return qrAreas[0];
        }
        if (minDiffs[1] > minDiffs[0] && minDiffs[1] > minDiffs[2]) {
            //Fringe ones are 0th and 2nd
            intersectPoint = qrAreas[1].getLineIntersection(minSlopes[0], qrAreas[0].getQuadriCenterPoint(), minSlopes[2], qrAreas[2].getQuadriCenterPoint());
            qrArea = new QRPolygon(new Point[]{qrAreas[1].getQuadriCenterPoint(), qrAreas[0].getQuadriCenterPoint(), intersectPoint, qrAreas[2].getQuadriCenterPoint()});
            //return qrAreas[1];
        }
        if (minDiffs[2] > minDiffs[1] && minDiffs[2] > minDiffs[0]) {
            //Fringe ones are 0th and 1st
            intersectPoint = qrAreas[2].getLineIntersection(minSlopes[1], qrAreas[1].getQuadriCenterPoint(), minSlopes[0], qrAreas[0].getQuadriCenterPoint());
            qrArea = new QRPolygon(new Point[]{qrAreas[2].getQuadriCenterPoint(), qrAreas[1].getQuadriCenterPoint(), intersectPoint, qrAreas[0].getQuadriCenterPoint()});
            //return qrAreas[2];
        }
        //return qrArea;//polyAreaList.add(qrArea);
        //polyAreaList.add(qrTriangle);
        Point centerPoint = qrArea.getQuadriCenterPoint();
        if (minDiffs[0] > minDiffs[2] && minDiffs[0] > minDiffs[1]) {
            //Fringe ones are 2nd and 1st
            intersectPoint = qrAreas[0].getLineIntersection(minSlopes[2], qrAreas[2].getFarthestPoint(centerPoint), minSlopes[1], qrAreas[1].getFarthestPoint(centerPoint));
            qrArea = new QRPolygon(new Point[]{qrAreas[0].getFarthestPoint(centerPoint), qrAreas[1].getFarthestPoint(centerPoint), intersectPoint, qrAreas[2].getFarthestPoint(centerPoint)});
        }
        if (minDiffs[1] > minDiffs[0] && minDiffs[1] > minDiffs[2]) {
            //Fringe ones are 0th and 2nd
            intersectPoint = qrAreas[1].getLineIntersection(minSlopes[0], qrAreas[0].getFarthestPoint(centerPoint), minSlopes[2], qrAreas[2].getFarthestPoint(centerPoint));
            qrArea = new QRPolygon(new Point[]{qrAreas[1].getFarthestPoint(centerPoint), qrAreas[0].getFarthestPoint(centerPoint), intersectPoint, qrAreas[2].getFarthestPoint(centerPoint)});

        }
        if (minDiffs[2] > minDiffs[1] && minDiffs[2] > minDiffs[0]) {
            //Fringe ones are 0th and 1st
            intersectPoint = qrAreas[2].getLineIntersection(minSlopes[1], qrAreas[1].getFarthestPoint(centerPoint), minSlopes[0], qrAreas[0].getFarthestPoint(centerPoint));
            qrArea = new QRPolygon(new Point[]{qrAreas[2].getFarthestPoint(centerPoint), qrAreas[1].getFarthestPoint(centerPoint), intersectPoint, qrAreas[0].getFarthestPoint(centerPoint)});
        }
        //polyAreaList.add(qrArea);
        return qrArea;
        //return qrArea.grow(qrAreas);
    }

}
