package qrcodereader;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import visionlibrary.QRAlgoStep;
import visionlibrary.QRCodeData;

/**
 * The QRAlgoColoredDirection class is a helpful algorithm step that colors
 * an image using a passed in directional derivative image. It is helpful for determining
 * what direction each pixel in the image has.
 *
 * @author daviesj
 */
public class QRAlgoColoredDirection extends QRAlgoStep {

    private QRAlgoStep dirStep;
    private QRAlgoStep gradStep;
    private int gradAbsMin;

    /**
     * The QRAlgoColoredDirection constructor requires a dirStep and gradStep image
     * that represent the directional and intensity derivatives at each pixel,
     * scaled to be from 0-255.
     * The gradAbsMin determines the minimum gradient intensity a pixel must
     * have in order to even be considered as having a direction.
     *
     * @param dirStep    The directional derivative image.
     * @param gradStep   The gradient derivative image.
     * @param gradAbsMin The minimum value in order to consider a pixel as even being on an edge.
     */
    public QRAlgoColoredDirection(QRAlgoStep dirStep, QRAlgoStep gradStep, int gradAbsMin) {
        this.dirStep = dirStep;
        this.gradStep = gradStep;
        this.gradAbsMin = gradAbsMin;
    }

    /**
     * This returns an image where the vertical(ish) pixels are colored blue,
     * the horizontal(ish) are pink, and the yellow are about NorthEast to SouthWest
     * and the green are SouthEast to NorthWest (these last two might be flipped)
     *
     * @return An image with colors at pixels that pass the minimum gradient value.
     */
    @Override
    public QRCodeData completeStep() {
        QRCodeData dirData = dirStep.getData();
        QRCodeData gradData = gradStep.getData();
        QRCodeData coloredData = new QRCodeData(new ArrayList());
        if (dirData != null && gradData != null) {
            for (int curImgIndex = 0; curImgIndex < dirData.getImages().size(); curImgIndex++) {
                BufferedImage dirImg = dirData.getImages().get(curImgIndex);
                BufferedImage gradImg = gradData.getImages().get(curImgIndex);
                BufferedImage coloredImg = new BufferedImage(dirImg.getWidth(), dirImg.getHeight(), dirImg.getType());
                for (int i = 0; i < dirImg.getWidth(); i++) {
                    for (int j = 0; j < dirImg.getHeight(); j++) {
                        int gradPixel = (new Color(gradImg.getRGB(i, j))).getRed();
                        if (gradPixel > gradAbsMin) {
                            int dirPixel = (new Color(dirImg.getRGB(i, j))).getRed() / 16;
                            Color setColor = new Color(0, 0, 0);
                            switch (dirPixel) {
                                case 0:
                                case 7:
                                case 8:
                                case 15:
                                    setColor = Color.BLUE;
                                    break;
                                case 1:
                                case 2:
                                case 9:
                                case 10:
                                    setColor = Color.GREEN;
                                    break;
                                case 3:
                                case 4:
                                case 11:
                                case 12:
                                    setColor = Color.PINK;
                                    break;
                                case 5:
                                case 6:
                                case 13:
                                case 14:

                                    setColor = Color.YELLOW;
                                    break;

                            }
                            coloredImg.setRGB(i, j, setColor.getRGB());
                        }
                    }
                }
                coloredData.addImage(coloredImg);
            }
        }
        result = coloredData;
        return coloredData;
    }

}
