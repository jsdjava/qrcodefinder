package qrcodereader;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import visionlibrary.QRAlgoStep;
import visionlibrary.QRCodeData;

/**
 * The QRAlgoCornerFilter marks pixels that look like they are probably a corner
 * using a gaussian like approach where it checks the neighbors, and if there is a certain
 * amount of difference between them it marks the pixel as a corner. It currently isn't used
 * in the final algorithm since it wasn't that useful.
 *
 * @author daviesj
 */
public class QRAlgoCornerFilter extends QRAlgoStep {

    private QRAlgoStep lineFilterStep;
    private QRAlgoStep dirStep;
    private int cornerRegion;
    private int minPixelDiff;

    /**
     * This requires an image where the pixels have been filtered to make sure they are
     * actually members of a line, and an image that describes the directions at each
     * pixel. Finally, it needs a region to search around the current pixel, and the minimum
     * difference in the region to mark the pixel as a corner.
     *
     * @param lineFilterStep An image where the pixels have been filtered using Canny Edge detection, and then also filtered to be members of a line.
     * @param dirStep        An image that describes the direction at each pixel
     * @param cornerRegion   The region to search around the pixel for pixels in other directions.
     * @param minPixelDiff   The minimu difference that must be found around the pixel in order to mark it as a corner.
     */
    public QRAlgoCornerFilter(QRAlgoStep lineFilterStep, QRAlgoStep dirStep, int cornerRegion, int minPixelDiff) {
        this.lineFilterStep = lineFilterStep;
        this.dirStep = dirStep;
        this.cornerRegion = cornerRegion;
        this.minPixelDiff = minPixelDiff;
    }

    /**
     * This returns an image where only prior edge pixels that appear to be corners
     * are marked.
     *
     * @return An image with only corner pixels marked.
     */
    @Override
    public QRCodeData completeStep() {
        QRCodeData squareFilteredImages = new QRCodeData(new ArrayList());
        QRCodeData lineFilterData = lineFilterStep.getData();
        QRCodeData dirData = dirStep.getData();
        if (lineFilterData != null && dirData != null) {
            for (int curImgIndex = 0; curImgIndex < dirData.getImages().size(); curImgIndex++) {
                BufferedImage lineFilterImg = lineFilterData.getImages().get(curImgIndex);
                BufferedImage dirImg = dirData.getImages().get(curImgIndex);
                BufferedImage newImg = new BufferedImage(dirImg.getWidth(), dirImg.getHeight(), dirImg.getType());
                for (int i = 0; i < lineFilterImg.getWidth(); i++) {
                    for (int j = 0; j < lineFilterImg.getHeight(); j++) {
                        if ((new Color(lineFilterImg.getRGB(i, j))).getRed() > 0) {
                            int minPixel = -1;
                            int maxPixel = -1;
                            for (int regionX = i - cornerRegion; regionX < i + cornerRegion + 1; regionX++) {
                                for (int regionY = j - cornerRegion; regionY < j + cornerRegion + 1; regionY++) {
                                    if (regionX >= 0 && regionX < dirImg.getWidth() && regionY >= 0 && regionY < dirImg.getHeight() && new Color(lineFilterImg.getRGB(regionX, regionY)).getRed() > 0) {
                                        int cornerPixel = (new Color(dirImg.getRGB(regionX, regionY))).getRed();
                                        if (minPixel < 0 || cornerPixel < minPixel) {
                                            minPixel = cornerPixel;
                                        }
                                        if (maxPixel < 0 || cornerPixel > maxPixel) {
                                            maxPixel = cornerPixel;
                                        }
                                    }
                                }
                            }
                            if (maxPixel - minPixel > minPixelDiff) {
                                newImg.setRGB(i, j, lineFilterImg.getRGB(i, j));
                            }
                        }
                    }
                }
                squareFilteredImages.addImage(newImg);
            }
        }
        result = squareFilteredImages;
        return squareFilteredImages;
    }

}
