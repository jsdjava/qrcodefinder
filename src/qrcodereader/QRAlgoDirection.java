package qrcodereader;

import visionlibrary.QRAlgoStep;

/**
 * The QRAlgoDirection class is useful for computing the directional value of a
 * pixel by using atan2 of the derivative in the y and the derivative in the x,
 * and then scaling it down to fit from 0-255 for 0-360.
 *
 * @author daviesj
 */
public class QRAlgoDirection extends QRAlgoGradient {

    /**
     * Requires a size for the size of the derivative filter to use (so how far out
     * to check neighbors). A size of 1 means it checks the pixel behind and after it,
     * and the one above and below it. A size of 2 means it checks 2 pixels behind and 2 pixels after
     * as well as 2 above and 2 below.
     *
     * @param size     The number of pixels to check in the back and front of the current pixel.
     * @param prevStep An image that has preferably been Gaussian Blurred to reduce noise.
     */
    public QRAlgoDirection(int size, QRAlgoStep prevStep) {
        super(size, prevStep);
    }

    /**
     * Returns the value at a specific pixel to set the output image to. This was useful
     * because I had already written QRAlgoGradient, and it did everything this filter needed
     * to do except it returned the magnitude of the derivative vector instead of the
     * direction represented by it.
     *
     * @param yDeriv The change in y computed at the current pixel.
     * @param xDeriv The change in x computed at the current pixel.
     * @return A pixel value representing the angle of the edge at this pixel, scaled from 0-255.
     */
    @Override
    protected int computePixelValue(int yDeriv, int xDeriv) {
        int dirDeriv = (int) ((Math.atan2(((double) yDeriv), ((double) xDeriv)) + Math.PI) * (255.0f / (2.0f * Math.PI)));
        return dirDeriv;
    }

}
