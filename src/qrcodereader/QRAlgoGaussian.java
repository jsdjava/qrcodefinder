package qrcodereader;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import visionlibrary.QRAlgoStep;
import visionlibrary.QRCodeData;

/**
 * This returns an image that has been blurred by a certain amount. It requires
 * a grayscale image be passed in.
 *
 * @author daviesj
 */
public class QRAlgoGaussian extends QRAlgoStep {

    private int size;
    private QRAlgoStep prevStep;

    /**
     * This creates a new Gaussian Filter. It requires that the passed in algo step
     * has gray scaled the image at some point.
     * The size represents the number of pixels behind and in front of this pixel to
     * accumulate, as well as above and below it. A size of 1 means that it uses a 3x3
     * filter, a size of 2 means it uses a 5x5 filter, etc.
     *
     * @param size     The number of pixels above, below, behind, and in front of the current one to accumulate into one sum.
     * @param prevStep An algorithm that preferably at some point produced a gray scale image.
     */
    public QRAlgoGaussian(int size, QRAlgoStep prevStep) {
        this.size = size;
        this.prevStep = prevStep;
    }

    /**
     * Creates an image that has been appropriately blurred according to the passed in size.
     * This is probably the slowest possible way I can think of to compute a Gaussian, if I were
     * to improve this I would start by rewriting this using some kind of linear algebra library
     * because this is just four nested for loops per image to compute.
     *
     * @return The blurred image.
     */
    @Override
    public QRCodeData completeStep() {
        QRCodeData blurredImages = new QRCodeData(new ArrayList());
        QRCodeData images = prevStep.getData();
        if (images != null) {
            for (BufferedImage curImg : images.getImages()) {
                BufferedImage newImg = new BufferedImage(curImg.getWidth(), curImg.getHeight(), curImg.getType());
                for (int i = 0; i < curImg.getWidth(); i++) {
                    for (int j = 0; j < curImg.getHeight(); j++) {
                        int gaussCount = 0;
                        int gaussSum = 0;
                        for (int gaussI = i - size; gaussI < i + size + 1; gaussI++) {
                            for (int gaussJ = j - size; gaussJ < j + size + 1; gaussJ++) {
                                if (gaussI >= 0 && gaussI < curImg.getWidth() && gaussJ >= 0 && gaussJ < curImg.getHeight()) {
                                    gaussSum += (new Color(curImg.getRGB(gaussI, gaussJ))).getRed();
                                    gaussCount++;
                                }
                            }
                        }
                        int gaussAvg = gaussSum / gaussCount;
                        newImg.setRGB(i, j, new Color(gaussAvg, gaussAvg, gaussAvg).getRGB());
                    }
                }
                blurredImages.addImage(newImg);
            }
        }
        result = blurredImages;
        return blurredImages;
    }

}
