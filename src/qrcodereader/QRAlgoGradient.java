package qrcodereader;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import visionlibrary.QRAlgoStep;
import visionlibrary.QRCodeData;

/**
 * The QRAlgoGradient algorithm step computes the magnitude of the x and y
 * derivatives at each pixel based on the size of the derivative filter passed in,
 * and returns this as an image, scaled from 0 to 255.
 *
 * @author daviesj
 */
public class QRAlgoGradient extends QRAlgoStep {

    private int size;
    private QRAlgoStep prevStep;

    /**
     * This constructor requires a size that is the number of pixels in front and behind this
     * pixel to include inside the derivative calculation. A size of 1 means it uses a 1x3 filter in the X
     * and a 1x3 filter in the y. A size of 2 means it uses a 1x5 in the X and Y directions, etc.
     * Note that I think this derivative computation could be improved using something I saw on wikipedia
     * that uses a 3x3 filter.
     *
     * @param size     The number of pixels in front of and behind, or above and below the current pixel to include in the derivative computation.
     * @param prevStep The previous algorithm uses, hopefully one that blurs it at some point as well as gray scaling so that noise is reduced, and color isn't a factor.
     */
    public QRAlgoGradient(int size, QRAlgoStep prevStep) {
        this.prevStep = prevStep;
        this.size = size;
    }

    /**
     * This computes a gradient image by determining what the derivative in the x and y of each pixel
     * is and then determining the sqrt((dx)^2+(dy)^2), and finally scaling this back to be in the range
     * from 0-255 in order to ensure that an output image can be created. I'm pretty sure I can improve
     * this first by using a linear algebra library to actually calculate the derivatives since this allows
     * for a lot of optimization, and also, I don't think that the sqrt operation is necessary, it might just
     * be better to add the two together.
     *
     * @return An image that shows the intensities of the derivatives at each point in the pixel.
     */
    @Override
    public QRCodeData completeStep() {
        QRCodeData derivativeImages = new QRCodeData(new ArrayList());
        QRCodeData startData = prevStep.getData();
        if (startData != null) {
            for (BufferedImage baseImg : startData.getImages()) {
                BufferedImage newImg = new BufferedImage(baseImg.getWidth(), baseImg.getHeight(), baseImg.getType());
                for (int i = 0; i < baseImg.getWidth(); i++) {
                    for (int j = 0; j < baseImg.getHeight(); j++) {
                        int derivSum = 0;
                        int derivSizeNeg = 0;
                        int derivSizePos = 0;
                        for (int derivI = i - size; derivI < i + size + 1; derivI++) {
                            if (derivI >= 0 && derivI < baseImg.getWidth()) {
                                derivSum += (derivI - i) * (new Color(baseImg.getRGB(derivI, j))).getRed();
                                if (derivI - i < 0) {
                                    derivSizeNeg += Math.abs(derivI - i);
                                } else if (derivI - i > 0) {
                                    derivSizePos += Math.abs(derivI - i);
                                }
                            }
                        }
                        int derivSizeMax = (derivSizeNeg > derivSizePos) ? derivSizeNeg : derivSizePos;
                        int xDeriv = derivSum / derivSizeMax;
                        derivSum = 0;
                        derivSizeNeg = 0;
                        derivSizePos = 0;
                        for (int derivJ = j - size; derivJ < j + size + 1; derivJ++) {
                            if (derivJ >= 0 && derivJ < baseImg.getHeight()) {
                                derivSum += (derivJ - j) * (new Color(baseImg.getRGB(i, derivJ))).getRed();
                                if (derivJ - j < 0) {
                                    derivSizeNeg += Math.abs(derivJ - j);
                                } else if (derivJ - j > 0) {
                                    derivSizePos += Math.abs(derivJ - j);
                                }
                            }
                        }
                        derivSizeMax = (derivSizeNeg > derivSizePos) ? derivSizeNeg : derivSizePos;
                        int yDeriv = derivSum / derivSizeMax;
                        int gradDeriv = computePixelValue(yDeriv, xDeriv);
                        newImg.setRGB(i, j, new Color(gradDeriv, gradDeriv, gradDeriv).getRGB());
                    }
                }
                derivativeImages.addImage(newImg);
            }
        }
        result = derivativeImages;
        return derivativeImages;
    }

    /**
     * This is overriden by the QRCodeDirection filter to do a different computation.
     * Basically, this is called at each pixel with the computed x and y derivatives,
     * and it returns sqrt((dx)^2+(dy)^2), and it attempts to scale it back into 0-255
     * range by dividing by sqrt(2) as well. One stupidly easy optimization would be to just
     * store the sqrt(2), or use 1.4 as an approximation since this computes it at every single pixel
     * over and over again.
     *
     * @param yDeriv The y derivative computed at the current pixel
     * @param xDeriv The x derivative computed at the current pixel
     * @return The value to set the pixel in the output image to
     */
    protected int computePixelValue(int yDeriv, int xDeriv) {
        int gradDeriv = (int) (Math.sqrt(((double) xDeriv * xDeriv + yDeriv * yDeriv)) / Math.sqrt(2));
        return gradDeriv;
    }
}
