package qrcodereader;

import java.awt.Color;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import visionlibrary.QRAlgoStep;
import visionlibrary.QRCodeData;

/**
 * This filters an image down to one where only edge pixels that are on lines of a
 * required minimum length are allowed through. This class is only used by being extended
 * by the square filter, the base algorithm isn't used since it just seemed extraneous,
 * and didn't reduce noise enough.
 *
 * @author daviesj
 */
public class QRAlgoLineFilter extends QRAlgoStep {

    private QRAlgoStep gradStep;
    private QRAlgoStep dirStep;
    protected int lineSize;
    protected List<Point> lineSearchPositions;

    /**
     * Requires that a minimum line size be passed in, as well as an image where at least the magnitude of the derivative
     * at each pixel is passed in, although its probably better if this image has also had suppression applied to it.It also
     * needs an image that represents the angle component of the derivative vector at each pixel.
     *
     * @param lineSize The minimum length of the line the pixel is a member of in order to qualify it.
     * @param gradStep An image with at least derivative magnitudes, although hopefully it has also suppression applied to it.
     * @param dirStep  An image that represents the angle of the derivative at each pixel.
     */
    public QRAlgoLineFilter(int lineSize, QRAlgoStep gradStep, QRAlgoStep dirStep) {
        this.gradStep = gradStep;
        this.dirStep = dirStep;
        this.lineSize = lineSize;
    }

    /**
     * This just loops through each pixel inside the gradient image, and applies handle pixel to each one.
     * In this class, though, it returns an image where if a pixel passes the gradient check, it is only marked
     * if it is a member of a line that is long enough.
     *
     * @return An image where only edge pixels that are on sufficiently long lines are marked.
     */
    @Override
    public QRCodeData completeStep() {
        QRCodeData lineFilteredImages = new QRCodeData(new ArrayList());
        QRCodeData gradStepData = gradStep.getData();
        QRCodeData dirStepData = dirStep.getData();
        if (gradStepData != null && dirStepData != null) {
            for (int curImgIndex = 0; curImgIndex < gradStepData.getImages().size(); curImgIndex++) {
                BufferedImage gradImg = gradStepData.getImages().get(curImgIndex);
                BufferedImage dirImg = dirStepData.getImages().get(curImgIndex);
                BufferedImage lineFilteredImg = new BufferedImage(dirImg.getWidth(), dirImg.getHeight(), dirImg.getType());
                for (int i = 0; i < gradImg.getWidth(); i++) {
                    for (int j = 0; j < gradImg.getHeight(); j++) {
                        handlePixel(i, j, curImgIndex, dirImg, gradImg, lineFilteredImg);
                    }
                }
                lineFilteredImages.addImage(lineFilteredImg);
            }
        }
        result = lineFilteredImages;
        return lineFilteredImages;
    }

    /**
     * This is useful for determining what to set in the output image based on the current pixel. It is used because this class
     * is overrided by the Square Filter with a different filter for the handle pixel method. This actual function basically calls line search
     * in the specified direction of each pixel that has a gradient value greater than 0 , and if the line search is long enough, it marks the pixel.
     *
     * @param i               The current x position of the pixel in the base img.
     * @param j               The current y position of the pixel in the base img.
     * @param curImg          The index of the current image in the list of images.
     * @param dirImg          An image that represents the directional derivative of each pixel in the image.
     * @param gradImg         An image that represents the magnitude derivative at each pixel in the image.
     * @param lineFilteredImg The output image to set the pixels in.
     */
    protected void handlePixel(int i, int j, int curImg, BufferedImage dirImg, BufferedImage gradImg, BufferedImage lineFilteredImg) {
        int gradPixel = new Color(gradImg.getRGB(i, j)).getRed();
        if (gradPixel > 0) { //Check it to see if its a line
            int curDir8Buckets = (new Color(dirImg.getRGB(i, j))).getRed() / 16;
            switch (curDir8Buckets) {
                case 3:
                case 4:
                case 11:
                case 12:
                    //Search right and left
                    int lineSearchLeft = lineSearch(gradImg, dirImg, i, j, -1, 0, new boolean[][]{{true, false, false}, {true, false, false}, {true, false, false}})[0];
                    int lineSearchRight = lineSearch(gradImg, dirImg, i, j, 1, 0, new boolean[][]{{false, false, true}, {false, false, true}, {false, false, true}})[0];
                    if (lineSearchLeft + lineSearchRight >= lineSize) {
                        lineFilteredImg.setRGB(i, j, new Color(gradPixel, gradPixel, gradPixel).getRGB());
                    }
                    break;
                case 1:
                case 2:
                case 9:
                case 10:
                    //Search the northeast and southwest
                    int lineSearchNorthEast = lineSearch(gradImg, dirImg, i, j, 1, -1, new boolean[][]{{false, true, true}, {false, false, true}, {false, false, false}})[0];
                    int lineSearchSouthWest = lineSearch(gradImg, dirImg, i, j, -1, 1, new boolean[][]{{false, false, false}, {true, false, false}, {true, true, false}})[0];
                    if (lineSearchNorthEast + lineSearchSouthWest >= lineSize) {
                        lineFilteredImg.setRGB(i, j, new Color(gradPixel, gradPixel, gradPixel).getRGB());
                    }
                    break;
                case 0:
                case 7:
                case 8:
                case 15:
                    //Search up and down
                    int lineSearchUp = lineSearch(gradImg, dirImg, i, j, 0, -1, new boolean[][]{{true, true, true}, {false, false, false}, {false, false, false}})[0];
                    int lineSearchDown = lineSearch(gradImg, dirImg, i, j, 0, 1, new boolean[][]{{false, false, false}, {false, false, false}, {true, true, true}})[0];
                    if (lineSearchUp + lineSearchDown >= lineSize) {
                        lineFilteredImg.setRGB(i, j, new Color(gradPixel, gradPixel, gradPixel).getRGB());
                    }
                    break;
                case 5:
                case 6:
                case 13:
                case 14:
                    //Search to the northwest and southeast
                    int lineSearchNorthWest = lineSearch(gradImg, dirImg, i, j, -1, -1, new boolean[][]{{true, true, false}, {true, false, false}, {false, false, false}})[0];
                    int lineSearchSouthEast = lineSearch(gradImg, dirImg, i, j, 1, 1, new boolean[][]{{false, false, false}, {false, false, true}, {false, true, true}})[0];
                    if (lineSearchNorthWest + lineSearchSouthEast >= lineSize) {
                        lineFilteredImg.setRGB(i, j, new Color(gradPixel, gradPixel, gradPixel).getRGB());
                    }
                    break;
            }
        }
    }

    /**
     * This searches starting at a specified pixel, in a specified direction, for a line by checking pixels in the neighborhood
     * around it based on the specified pixels in that neighborhood to check. It picks a new one based on how close its directional
     * derivative is to the previous one's, and then continues the seacrch from there until it runs out of line. This does allow a line
     * to fail by one pixel, just in case the blur causes one pixel in the middle or w/e to not get set when it should be. Basically, it
     * takes two failed pixels to determine that the line has ended. It then returns the length of the line, as well as the end position
     * in the x and y of the line.
     *
     * @param img         An image with the magnitude of the derivative at each pixel in the base image.
     * @param dirImg      An image with the angle of the derivative at each pixel in the base image.
     * @param startX      The x position to start searching for the line at.
     * @param startY      The y position to start searching for the line at.
     * @param defaultXInc If no pixel is found for any of the neighborhood pixels, this is the amount the increment in the x by before checking again.
     * @param defaultYInc Same as above except for the y.
     * @param searchDir   This is a 3x3 array of booleans that determines whether each pixel in the neighborhood of the current line one should be checked.
     *                    Its basically [NorthWest, North, NorthEast],[West,Same Pixel(should never be set), East],[SouthWest,South,SouthEast]
     * @return an int arry that is [length of the line, x position of the end of the line, y position of the end of the line]
     */
    protected int[] lineSearch(BufferedImage img, BufferedImage dirImg, int startX, int startY, int defaultXInc, int defaultYInc, boolean[][] searchDir) {
        return lineSearch(img, dirImg, startX, startY, defaultXInc, defaultYInc, searchDir, false);
    }

    /**
     * This is the exact same as the method above, so I'm not recommenting it. The only difference is that the
     * store all positions boolean is allowed through. Basically, if you want every single pixel that is on the line to be saved inside
     * the line search positions arraylist, set this to true. Note that this arraylist is cleared every time lineSearch is called, so be sure to save all its elements
     * somewhere else before calling line search again.
     *
     * @param gradImg
     * @param dirImg
     * @param startX
     * @param startY
     * @param defaultXInc
     * @param defaultYInc
     * @param searchDir
     * @param storeAllPositions
     * @return
     */
    protected int[] lineSearch(BufferedImage gradImg, BufferedImage dirImg, int startX, int startY, int defaultXInc, int defaultYInc, boolean[][] searchDir, boolean storeAllPositions) {
        int lineLength = 0;
        int failPixelCount = 0;
        int newX = -1;
        int newY = -1;
        lineSearchPositions = new ArrayList();
        lineSearchPositions.add(new Point(startX, startY));
        int curPixelDir = new Color(dirImg.getRGB(startX, startY)).getRed();
        while (failPixelCount < 2) {
            int maxPixel = 0;
            int maxPixelDir = 255;
            for (int i = -1; i < 2; i++) {
                for (int j = -1; j < 2; j++) {
                    if (searchDir[j + 1][i + 1]) {
                        int checkX = startX + i;
                        int checkY = startY + j;
                        if (checkX >= 0 && checkX < gradImg.getWidth() && checkY >= 0 && checkY < gradImg.getHeight()) {
                            int checkPixel = new Color(gradImg.getRGB(checkX, checkY)).getRed();
                            int checkPixelDir = (new Color(dirImg.getRGB(checkX, checkY))).getRed();
                            if (checkPixel > 0 && (maxPixelDir < 0 || Math.abs(checkPixelDir - curPixelDir) < maxPixelDir)) {
                                maxPixel = checkPixel;
                                maxPixelDir = Math.abs(checkPixelDir - curPixelDir);
                                newX = checkX;
                                newY = checkY;
                            }
                        }

                    }
                }
            }
            if (maxPixel > 0) {
                if (storeAllPositions) {
                    lineSearchPositions.add(new Point(newX, newY));
                }
                failPixelCount = 0;
                startX = newX;
                startY = newY;
                curPixelDir = new Color(dirImg.getRGB(startX, startY)).getRed();
            } else {
                startX += defaultXInc;
                startY += defaultYInc;
                failPixelCount++;
            }
            lineLength++;
        }
        return new int[]{lineLength, newX, newY};
    }
}
