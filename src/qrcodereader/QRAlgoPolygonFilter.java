package qrcodereader;

import java.util.ArrayList;
import java.util.List;
import visionlibrary.QRAlgoStep;
import visionlibrary.QRCodeData;
import visionlibrary.QRPolygon;

/**
 * This filters passed in polygons so that only polygons that contain exactly two
 * other polygons are allowed through. It then returns these filtered polygons in a new
 * list of polygons.
 *
 * @author daviesj
 */
public class QRAlgoPolygonFilter extends QRAlgoStep {

    private QRAlgoStep squareFilterStep;

    /**
     * Requires that a previous algorithm that results in a list of polygons is returned, preferably the square filter
     * that creates squares out of the appropriate pixels.
     *
     * @param squareFilterStep An algo step that results in a list of polygon objects.
     */
    public QRAlgoPolygonFilter(QRAlgoStep squareFilterStep) {
        this.squareFilterStep = squareFilterStep;
    }

    /**
     * This returns a filtered list of polygons as long as the previous algorithm gives it a list of polygons.
     * If the previous algorithm doesn't return a list of polygons, it just returns an empty list of polygons.
     * The filter applied is that it checks to see if the current polygon contains exactly two other polygons, and if it
     * does, it allows it through. This could be improved by not only checking to see if it contains two, but also seeing if one of the inner
     * rectangles contains the other one, but could be dangerous because sometimes these rectangles are so close they are almost intersecting.
     * Also, this is an O(n^2) operation, where n is the number of polygons, so you better not pass more than 500ish polygons in here if you want
     * it to complete quickly.
     *
     * @return A list of polygons where only polygons that contain exactly two other polygons in their bounding boxes are allowed through.
     */
    @Override
    public QRCodeData completeStep() {
        QRCodePolygons polygonsStart = new QRCodePolygons();
        QRCodePolygons polygonsFiltered = new QRCodePolygons();
        if (squareFilterStep instanceof QRAlgoSquareFilter) {
            polygonsStart = ((QRAlgoSquareFilter) squareFilterStep).getPolygonData();
            for (List<QRPolygon> polygons : polygonsStart.getPolygons()) {
                boolean[] isAdded = new boolean[polygons.size()];
                int upperPolyCount = 0;
                List<QRPolygon> curPolygonsFiltered = new ArrayList();
                for (QRPolygon upperPoly : polygons) {
                    if (!isAdded[upperPolyCount]) {
                        List<Integer> containedPolygonIndices = new ArrayList();
                        int curPolyCount = 0;
                        for (QRPolygon lowerPoly : polygons) {
                            if (!upperPoly.equals(lowerPoly) && (!isAdded[curPolyCount])) {
                                if (upperPoly.getRectangle().contains(lowerPoly.getRectangle())) {
                                    containedPolygonIndices.add(curPolyCount);
                                }
                            }
                            curPolyCount++;
                        }
                        if (containedPolygonIndices.size() == 2) {
                            for (int curPolyIndex : containedPolygonIndices) {
                                isAdded[curPolyIndex] = true;
                            }
                            if (polygons.get(containedPolygonIndices.get(0)).getRectangle().contains(polygons.get(containedPolygonIndices.get(1)).getRectangle())) {
                                curPolygonsFiltered.add(polygons.get(containedPolygonIndices.get(1)));
                                curPolygonsFiltered.add(polygons.get(containedPolygonIndices.get(0)));
                                curPolygonsFiltered.add(upperPoly);

                            } else if (polygons.get(containedPolygonIndices.get(1)).getRectangle().contains(polygons.get(containedPolygonIndices.get(0)).getRectangle())) {
                                curPolygonsFiltered.add(polygons.get(containedPolygonIndices.get(0)));
                                curPolygonsFiltered.add(polygons.get(containedPolygonIndices.get(1)));
                                curPolygonsFiltered.add(upperPoly);
                            } else {
                                //Discount it...
                            }
                        }
                    }
                    upperPolyCount++;
                }
                polygonsFiltered.addPolygons(curPolygonsFiltered);
            }
        }
        result = polygonsFiltered;
        return polygonsFiltered;
    }

}
