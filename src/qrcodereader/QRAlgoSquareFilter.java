package qrcodereader;

import java.awt.Color;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import visionlibrary.QRAlgoStep;
import visionlibrary.QRCodeData;
import visionlibrary.QRPolygon;

/**
 * This returns an image where only pixels that are on edges and that appear to be members of a square
 * are allowed through.
 *
 * @author daviesj
 */
public class QRAlgoSquareFilter extends QRAlgoLineFilter {

    private QRAlgoStep cornerStep;
    private int percentClose;
    private int endLineClose;

    /**
     * This requires a lineSize, which is basically the minimum length of one side of the square, and images where the magnitude and angle of the derivative at each pixel
     * are marked, as well as a corner step, which just uses the gradient image currently so ignore it, and a percent close, which mines how close in length the
     * first two lines found on the pixel must be, and an endline close which determines how close the two endpoints found for the square must be to each other.
     *
     * @param lineSize     The minimum length of any side of the square in order to count it.
     * @param gradStep     An image that holds the magnitude of the derivative at each pixel.
     * @param dirStep      An image that holds the angle of the derivative at each pixel.
     * @param cornerStep   Exact same as the gradStep currently, should just be deleted.
     * @param percentClose This is how close the length of the first two lines that are found must be, as an int from 0-100.
     * @param endLineClose This is how close the two endpoints found by going cw and ccw around the square must be in order to count it as a square, in pixels.
     */
    public QRAlgoSquareFilter(int lineSize, QRAlgoStep gradStep, QRAlgoStep dirStep, QRAlgoStep cornerStep, int percentClose, int endLineClose) {
        super(lineSize, gradStep, dirStep);
        this.cornerStep = cornerStep;
        this.percentClose = percentClose;
        this.endLineClose = endLineClose;
        polygonData = new QRCodePolygons();
    }
    private BufferedImage squareImg = null;
    private int squareCount = 0;
    private QRCodePolygons polygonData;
    private List<QRPolygon> polygons;

    /**
     * Returns a list of polygons that can be used in other algorithms.
     * Think I could just override getData or something here, not sure if this is
     * necessary.
     *
     * @return A wrapper that holds a list (one index per image) of lists of polygons.
     */
    public QRCodePolygons getPolygonData() {
        return polygonData;
    }

    /**
     * This is called at each pixel in the image by the parent class.
     *
     * @param i               The current x position of the pixel in the base image.
     * @param j               The current y position of the pixel in the base image.
     * @param curImg          The current index of the image being processed.
     * @param dirImg          An image that holds the directional derivative at each pixel.
     * @param gradImg         An image that holds the magnitude of the derivative at each pixel.
     * @param lineFilteredImg The output image to mark square pixels in.
     */
    @Override
    protected void handlePixel(int i, int j, int curImg, BufferedImage dirImg, BufferedImage gradImg, BufferedImage lineFilteredImg) {
        QRCodeData cornerStepData = cornerStep.getData();
        if (i == 0 && j == 0) {
            squareImg = new BufferedImage(dirImg.getWidth(), dirImg.getHeight(), dirImg.getType());
            polygons = new ArrayList();
            squareCount = 0;
        }
        if (cornerStepData != null) {
            BufferedImage cornerImg = cornerStepData.getImages().get(curImg);
            int cornerPixel = new Color(cornerImg.getRGB(i, j)).getRed();
            if (cornerPixel > 0 && new Color(squareImg.getRGB(i, j)).getRed() == 0) { //Check it to see if its on/near a corner and hasn't already been marked as being part of a square
                List<Point> squarePixels = new ArrayList();
                List<Integer> listMarkers = new ArrayList();
                List<Point> tempPixels = new ArrayList();
                int lineLengths[] = new int[2];
                int[] xStart = new int[2];
                int[] yStart = new int[2];
                boolean searchLeft = false;
                //Search right and left
                int[] lineSearchResultsLeft = lineSearch(gradImg, dirImg, i, j, -1, 0, new boolean[][]{{true, false, false}, {true, false, false}, {true, false, false}}, true);
                tempPixels.addAll(lineSearchPositions);
                int[] lineSearchResultsRight = lineSearch(gradImg, dirImg, i, j, 1, 0, new boolean[][]{{false, false, true}, {false, false, true}, {false, false, true}}, true);
                if (lineSearchResultsLeft[0] + lineSearchResultsRight[0] >= lineSize) {
                    if (lineSearchResultsLeft[0] > lineSearchResultsRight[0]) {
                        squarePixels.addAll(tempPixels);
                    } else {
                        squarePixels.addAll(lineSearchPositions);
                    }
                    listMarkers.add(squarePixels.size());
                    lineLengths[0] = (lineSearchResultsLeft[0] > lineSearchResultsRight[0]) ? lineSearchResultsLeft[0] : lineSearchResultsRight[0];
                    xStart[0] = (lineSearchResultsLeft[0] > lineSearchResultsRight[0]) ? lineSearchResultsLeft[1] : lineSearchResultsRight[1];
                    yStart[0] = (lineSearchResultsLeft[0] > lineSearchResultsRight[0]) ? lineSearchResultsLeft[2] : lineSearchResultsRight[2];
                    searchLeft = lineSearchResultsLeft[0] > lineSearchResultsRight[0];
                }
                tempPixels.clear();
                //Search up and down
                int[] lineSearchResultsUp = lineSearch(gradImg, dirImg, i, j, 0, -1, new boolean[][]{{true, true, true}, {false, false, false}, {false, false, false}}, true);
                tempPixels.addAll(lineSearchPositions);
                int[] lineSearchResultsDown = lineSearch(gradImg, dirImg, i, j, 0, 1, new boolean[][]{{false, false, false}, {false, false, false}, {true, true, true}}, true);
                boolean searchUp = false;
                if (lineSearchResultsUp[0] + lineSearchResultsDown[0] >= lineSize) {
                    if (lineSearchResultsUp[0] > lineSearchResultsDown[0]) {
                        squarePixels.addAll(tempPixels);
                    } else {
                        squarePixels.addAll(lineSearchPositions);
                    }
                    listMarkers.add(squarePixels.size());
                    lineLengths[1] = (lineSearchResultsUp[0] > lineSearchResultsDown[0]) ? lineSearchResultsUp[0] : lineSearchResultsDown[0];
                    xStart[1] = (lineSearchResultsUp[0] > lineSearchResultsDown[0]) ? lineSearchResultsUp[1] : lineSearchResultsDown[1];
                    yStart[1] = (lineSearchResultsUp[0] > lineSearchResultsDown[0]) ? lineSearchResultsUp[2] : lineSearchResultsDown[2];
                    searchUp = lineSearchResultsUp[0] > lineSearchResultsDown[0];

                }
                if (lineLengths[1] > lineSize && lineLengths[0] > lineSize) {
                    double percentCloseDbl = ((double) percentClose) / 100.0f;
                    int greaterLine = lineLengths[0] > lineLengths[1] ? lineLengths[0] : lineLengths[1];
                    int lesserLine = lineLengths[0] < lineLengths[1] ? lineLengths[0] : lineLengths[1];
                    double linePercent = ((double) lesserLine) / ((double) greaterLine);
                    if (linePercent >= percentCloseDbl) {
                        //Extend the y pixel we found out
                        int newXLineResults[];
                        int newYLineResults[];
                        tempPixels.clear();
                        if (searchLeft) {
                            newXLineResults = lineSearch(gradImg, dirImg, xStart[1], yStart[1], -1, 0, new boolean[][]{{true, false, false}, {true, false, false}, {true, false, false}}, true);
                            tempPixels.addAll(lineSearchPositions);
                        } else {
                            newXLineResults = lineSearch(gradImg, dirImg, xStart[1], yStart[1], +1, 0, new boolean[][]{{false, false, true}, {false, false, true}, {false, false, true}}, true);
                            tempPixels.addAll(lineSearchPositions);
                        }
                        if (searchUp) {
                            newYLineResults = lineSearch(gradImg, dirImg, xStart[0], yStart[0], 0, -1, new boolean[][]{{true, true, true}, {false, false, false}, {false, false, false}}, true);
                        } else {
                            newYLineResults = lineSearch(gradImg, dirImg, xStart[0], yStart[0], 0, +1, new boolean[][]{{false, false, false}, {false, false, false}, {true, true, true}}, true);
                        }
                        if (newXLineResults[0] > lineSize && newYLineResults[0] > lineSize) {
                            Point endPointOne = new Point(newXLineResults[1], newXLineResults[2]);
                            Point endPointTwo = new Point(newYLineResults[1], newYLineResults[2]);
                            if (endPointOne.distance(endPointTwo) <= endLineClose) {
                                //We can also do a check for the size in here, not sure if thats useful at all if we have made it this far though
                                int foundSquareCount = 0;
                                squarePixels.addAll(tempPixels);
                                listMarkers.add(squarePixels.size());
                                squarePixels.addAll(lineSearchPositions);
                                listMarkers.add(squarePixels.size());
                                for (Point curPixel : squarePixels) {
                                    if ((new Color(squareImg.getRGB(curPixel.x, curPixel.y))).getRed() > 0) {
                                        foundSquareCount++;
                                    }
                                    squareImg.setRGB(curPixel.x, curPixel.y, new Color(255, 255, 255).getRGB());
                                }
                                if (foundSquareCount < lineSize) {
                                    List<Point>[] linePixels = new List[4];
                                    int prevPixelIndex = 0;
                                    for (int curLineIndex = 0; curLineIndex < listMarkers.size(); curLineIndex++) {
                                        linePixels[curLineIndex] = new ArrayList();
                                        for (int curPixelIndex = prevPixelIndex; curPixelIndex < listMarkers.get(curLineIndex); curPixelIndex++) {
                                            linePixels[curLineIndex].add(squarePixels.get(curPixelIndex));
                                        }
                                        prevPixelIndex = listMarkers.get(curLineIndex);
                                    }
                                    polygons.add(new QRPolygon(linePixels, new Point[]{new Point(i, j), new Point(xStart[1], yStart[1]), endPointOne, new Point(xStart[0], yStart[0])}));
                                    lineFilteredImg.setRGB(i, j, gradImg.getRGB(i, j));
                                    squareCount++;
                                }
                            }
                        }
                    }
                }
            }
        }
        if (i == gradImg.getWidth() - 1 && j == gradImg.getHeight() - 1) {
            if (polygons != null) {
                polygonData.addPolygons(polygons);
                System.out.println("squareCount " + squareCount);
            }
        }
    }
}
