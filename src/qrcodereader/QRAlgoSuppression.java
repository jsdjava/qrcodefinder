package qrcodereader;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import visionlibrary.QRAlgoStep;
import visionlibrary.QRCodeData;

/**
 * This class I basically wrote based off a description on wikipedia of Canny Edge Detection. Basically,
 * after you have blurred and computed the derivatives of each pixel in the grayscale image, you have edges that
 * are too thick because of the blurring. In order to reduce this thickness, you need to search perpendicular to the direction of the line
 * at each pixel, and only allow through the strongest one.
 *
 * @author daviesj
 */
public class QRAlgoSuppression extends QRAlgoStep {

    private QRAlgoStep gradStep;
    private QRAlgoStep dirStep;
    private int gradAbsMin;

    /**
     * This requires that the magnitude of the derivative at each pixel in the image is passed in, and that
     * the angle of the derivative at each pixel is also passed in, as well as the minimum value of the magnitude
     * of the derivative at a pixel to count it as even being a member of the edge.
     *
     * @param gradStep
     * @param dirStep
     * @param gradAbsMin
     */
    public QRAlgoSuppression(QRAlgoStep gradStep, QRAlgoStep dirStep, int gradAbsMin) {
        this.gradStep = gradStep;
        this.dirStep = dirStep;
        this.gradAbsMin = gradAbsMin;
    }

    /**
     * This uses the direction of the derivative at each pixel to determine if which pixels to check against it, and if those
     * pixels have greater magnitude values for their derivatives, then the current pixel isn't set to true.
     *
     * @return An image where only the strongest edge pixels are allowed through( so the blurry/weak ones are suppressed).
     */
    @Override
    public QRCodeData completeStep() {
        QRCodeData suppressedImages = new QRCodeData(new ArrayList());
        QRCodeData gradStepData = gradStep.getData();
        QRCodeData dirStepData = dirStep.getData();
        if (gradStepData != null && dirStepData != null) {
            for (int curImgIndex = 0; curImgIndex < gradStepData.getImages().size(); curImgIndex++) {
                BufferedImage gradImg = gradStepData.getImages().get(curImgIndex);
                BufferedImage dirImg = dirStepData.getImages().get(curImgIndex);
                BufferedImage suppImg = new BufferedImage(gradImg.getWidth(), gradImg.getHeight(), gradImg.getType());
                for (int i = 0; i < gradImg.getWidth(); i++) {
                    for (int j = 0; j < gradImg.getHeight(); j++) {
                        int gradPixel = (new Color(gradImg.getRGB(i, j))).getRed();
                        if (gradPixel > gradAbsMin) {
                            int curDir8Buckets = (new Color(dirImg.getRGB(i, j))).getRed() / 16;
                            switch (curDir8Buckets) {
                                case 3:
                                case 4:
                                case 11:
                                case 12:
                                    int northPixel = ((j - 1) >= 0) ? (new Color(gradImg.getRGB(i, j - 1))).getRed() : 0;
                                    int southPixel = ((j + 1) < gradImg.getHeight()) ? (new Color(gradImg.getRGB(i, j + 1))).getRed() : 0;
                                    if (gradPixel >= northPixel && gradPixel >= southPixel) {
                                        suppImg.setRGB(i, j, gradImg.getRGB(i, j));
                                    }
                                    break;
                                case 1:
                                case 2:
                                case 9:
                                case 10:
                                    int northWestPixel = ((j - 1) >= 0 && (i - 1) >= 0) ? (new Color(gradImg.getRGB(i - 1, j - 1))).getRed() : 0;
                                    int southEastPixel = ((j + 1) < gradImg.getHeight() && (i + 1) < gradImg.getWidth()) ? (new Color(gradImg.getRGB(i + 1, j + 1))).getRed() : 0;
                                    if (gradPixel >= northWestPixel && gradPixel >= southEastPixel) {
                                        suppImg.setRGB(i, j, gradImg.getRGB(i, j));
                                    }
                                    break;
                                case 0:
                                case 7:
                                case 8:
                                case 15:
                                    int eastPixel = ((i - 1) >= 0) ? (new Color(gradImg.getRGB(i - 1, j))).getRed() : 0;
                                    int westPixel = ((i + 1) < gradImg.getWidth()) ? (new Color(gradImg.getRGB(i + 1, j))).getRed() : 0;
                                    if (gradPixel >= eastPixel && gradPixel >= westPixel) {
                                        suppImg.setRGB(i, j, gradImg.getRGB(i, j));
                                    }
                                    break;
                                case 5:
                                case 6:
                                case 13:
                                case 14:
                                    int northEastPixel = ((j - 1) >= 0 && (i + 1) < gradImg.getWidth()) ? (new Color(gradImg.getRGB(i + 1, j - 1))).getRed() : 0;
                                    int southWestPixel = ((j + 1) < gradImg.getHeight() && (i - 1) >= 0) ? (new Color(gradImg.getRGB(i - 1, j + 1))).getRed() : 0;
                                    if (gradPixel >= northEastPixel && gradPixel >= southWestPixel) {
                                        suppImg.setRGB(i, j, gradImg.getRGB(i, j));
                                    }
                                    break;
                            }
                        }
                    }
                }
                suppressedImages.addImage(suppImg);
            }
        }
        result = suppressedImages;
        return suppressedImages;
    }

}
