package qrcodereader;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.List;
import visionlibrary.QRAlgoStep;
import visionlibrary.QRCodeData;
import visionlibrary.QRPolygon;

/**
 * This takes in a list of polygons that should be the corners of the qr code, and
 * also the base image that has no filters applied to it, and just draws the polygons on top
 * of those images, and returns those images.
 *
 * @author daviesj
 */
public class QRAlgoTraceImage extends QRAlgoStep {

    private QRAlgoStep loadImgStep;
    private QRAlgoStep polygonFilterStep;

    /**
     * This requires that the base unfiltered images are passed in as an algorithm step, as well
     * as the polygons that have been filtered down to only be members of the QRCode corner rectangles.
     *
     * @param loadImgStep
     * @param polygonFilterStep
     */
    public QRAlgoTraceImage(QRAlgoStep loadImgStep, QRAlgoStep polygonFilterStep) {
        this.loadImgStep = loadImgStep;
        this.polygonFilterStep = polygonFilterStep;
    }

    /**
     * Traces the polygons on top of the base unfiltered images, and returns these as a new image.
     *
     * @return A list of images where the qr code regions have been traced in magenta on top of the base unfiltered image.
     */
    @Override
    public QRCodeData completeStep() {
        QRCodeData tracedImages = new QRCodeData();
        QRCodeData baseImgData = loadImgStep.getData();
        if (baseImgData != null && polygonFilterStep.getData() instanceof QRCodePolygons) {
            QRCodePolygons polygonData = (QRCodePolygons) polygonFilterStep.getData();
            if (polygonData != null) {
                for (int curImgIndex = 0; curImgIndex < polygonData.getPolygons().size(); curImgIndex++) {
                    List<QRPolygon> curPolygons = polygonData.getPolygons().get(curImgIndex);
                    BufferedImage baseImg = baseImgData.getImages().get(curImgIndex);
                    BufferedImage traceImg = new BufferedImage(baseImg.getWidth(), baseImg.getHeight(), BufferedImage.TYPE_INT_RGB);
                    Graphics traceGraphics = traceImg.getGraphics();
                    traceGraphics.drawImage(baseImg, 0, 0, null);
                    for (QRPolygon curPoly : curPolygons) {
                        curPoly.display(traceGraphics);
                    }
                    tracedImages.addImage(traceImg);
                }
            }
        }
        result = tracedImages;
        return tracedImages;
    }

}
