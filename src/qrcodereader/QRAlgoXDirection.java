package qrcodereader;

import visionlibrary.QRAlgoStep;

/**
 * This class is the exact same as the QRAlgoDirection algorithm, except that
 * it just returns the x derivative instead of any angle.
 *
 * @author daviesj
 */
public class QRAlgoXDirection extends QRAlgoDirection {

    /**
     * Requires that a blurred image is passed in, with a size for the derivative filter.
     *
     * @param size     The size of the derivative filter.
     * @param prevStep The previous algorithm to use as a base for this one, preferably a Gaussian one.
     */
    public QRAlgoXDirection(int size, QRAlgoStep prevStep) {
        super(size, prevStep);
    }

    /**
     * This just returns xDeriv instead of the angle at the pixel.
     *
     * @param yDeriv The x derivative at the current pixel.
     * @param xDeriv The y derivative at the current pixel.
     * @return The x derivative at the current pixel.
     */
    @Override
    protected int computePixelValue(int yDeriv, int xDeriv) {
        int dirDeriv = Math.abs(xDeriv);
        return dirDeriv;
    }

}
