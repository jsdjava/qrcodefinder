package qrcodereader;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;
import visionlibrary.QRCodeData;
import visionlibrary.QRPolygon;

/**
 * This holds a list of a list of polygon objects. It extends QRCodeData, but instead of holding
 * images, it holds a list of polygons.
 *
 * @author daviesj
 */
public class QRCodePolygons extends QRCodeData {

    private List<List<QRPolygon>> polygonsList;

    /**
     * Constructs a QRCodePolygons object with an empty list of lists of polygons.
     */
    public QRCodePolygons() {
        polygonsList = new ArrayList();
    }

    /**
     * Draws all the polygons in the currently selected list of polygons to the
     * passed in Graphics object.
     *
     * @param q      The graphics object to draw to.
     * @param width  The height of the canvas
     * @param height The width of the canvas.
     */
    @Override
    public void display(Graphics q, int width, int height) {
        if (imgItr >= 0 && imgItr < polygonsList.size()) {
            List<QRPolygon> polygons = polygonsList.get(imgItr);
            for (QRPolygon curPoly : polygons) {
                curPoly.display(q);
            }
        }
    }

    /**
     * Adds a list of polygons to the current list of polygons.
     *
     * @param polygons The new polygons list to add.
     */
    public void addPolygons(List<QRPolygon> polygons) {
        polygonsList.add(polygons);
    }

    /**
     * Returns the list of lists of polygons.
     *
     * @return The list of lists of polygons.
     */
    public List<List<QRPolygon>> getPolygons() {
        return polygonsList;
    }

    /**
     * Same as super
     */
    @Override
    public void nextImage() {
        if (imgItr < polygonsList.size() - 1) {
            imgItr++;
        }
    }

    /**
     * Same as super
     */
    @Override
    public void prevImage() {
        if (imgItr > 0) {
            imgItr--;
        }
    }

    /**
     * Same as super
     */
    @Override
    public void zeroImage() {
        imgItr = 0;
    }

    /** 
     * Same as super
     * @param spot 
     */
    @Override
    public void setImageSpot(int spot) {
        imgItr = spot;
    }

    /**
     * Same as super
     * @return 
     */
    @Override
    public int getImageSpot() {
        return imgItr;
    }
}
