package qrcodereader;

import visionlibrary.QRAlgoGrayscale;
import visionlibrary.AlgoController;
import java.util.ArrayList;
import java.util.List;
import visionlibrary.QRAlgoLoadImages;
import visionlibrary.QRAlgoStep;

/**
 * This is the main class in the project, it handles creating the GUI, and does high level setup
 * of the algorithms being run. It also deals with sending over the currently selected data to the
 * GUI so it can be displayed.
 *
 * @author daviesj
 */
public class QRCodeReader extends AlgoController {

    public static void main(String[] args) {
        new QRCodeReader();
    }

    /**
     * Sets up the QRCodePanel, and then runs through each different algorithm using the constructAlgorithmSequence in order to finally get the QRCode outlined on the final image.
     * 1.)Load the images from the current directory
     * 2.)Turn all the currently loaded images to grayscale
     * 3.)Blur all the images using a 3x3 matrix
     * 4.)Compute the magnitude of the derivative at each pixel using a 1x3 vertical and horizontal filter
     * 5.)Compute the angle of the derivative at each pixel using " "
     * 6.)Suppress the edge pixels in an image so only the strongest ones that are members of an edge are allowed through. Requires a minimum gradient of to consider a pixel to be an edge.
     * 7.)Finds pixels that are probably on squares and adds them to a list of polygons, using a line length similarity of 60%, and a maximum difference between the CW and CCW end pixels of the square of 5.
     * 8.)Only allows through polygons that contain exactly two other polygons.
     * 9.)Traces the found polygons on the base loaded images to mark the qr code corners in magenta.
     * Some notes on the final algorithm/ high level improvements.
     * I saw that there is a way to precompute the entire gaussian/derivative filters, and it would be way easier to use this in some sort of linear algebra library to speed the whole thing up.
     * Also, it is very translation invariant, but it is only scale invariant up until the minimum size of the line (Currently 10 pixels). If the innermost square of a qr code image is smaller than this, it won't
     * work. It is fairly rotation invariant, except I had to write the SquareFilter to search in the actual x and y directions instead of basing it on the current derivative at the pixel since that didn't seem to work.
     * This means that if it gets rotated too far it struggles a little bit. Finally, it should be completely color invariant (since it grayscales the entire thing), and it should be fairly illumination invarian since
     * it is only using derivatives, but I guess if the picture is way too dim it probably won't work.
     */
    public QRCodeReader() {
        super("QRCodeReader", new QRCodePanel(), true);

        qrAlgoSteps = constructAlgorithmSequence(new QRAlgoLoadImages("."));
        //qrAlgoSteps.add(new QRAlgoTraceImage(qrAlgoSteps.get(0), qrAlgoSteps.get(7)));
        //qrAlgoSteps.add(new QRAlgoColoredDirection(qrAlgoSteps.get(4), qrAlgoSteps.get(3), 25));
        //qrAlgoSteps.add(new QRAlgoYDirection(1, qrAlgoSteps.get(2)));
        // qrAlgoSteps.add(new QRAlgoXDirection(1, qrAlgoSteps.get(2)));
    }

    /**
     * This constructs the current sequence of algorithms that I use to find
     * QR Code images. There is a more detailed description of it above the constructor
     * in this class.
     *
     * @param loadStep The initial step that loads images from somewhere, like a webcam
     *                 or a directory.
     * @return a list of algorithm steps linked correctly in order to get the final
     *         QR Code rectangle area.
     */
    protected List<QRAlgoStep> constructAlgorithmSequence(QRAlgoStep loadStep) {
        List<QRAlgoStep> qrAlgoSteps = new ArrayList();
        qrAlgoSteps.add(loadStep); //0
        qrAlgoSteps.add(new QRAlgoGrayscale(qrAlgoSteps.get(0))); //1
        qrAlgoSteps.add(new QRAlgoGaussian(1, qrAlgoSteps.get(1))); //2
        qrAlgoSteps.add(new QRAlgoGradient(1, qrAlgoSteps.get(2))); // 3
        qrAlgoSteps.add(new QRAlgoDirection(1, qrAlgoSteps.get(2)));//4
        qrAlgoSteps.add(new QRAlgoSuppression(qrAlgoSteps.get(3), qrAlgoSteps.get(4), 25));//5
        //qrAlgoSteps.add(new QRAlgoLineFilter(15, qrAlgoSteps.get(5), qrAlgoSteps.get(4))); //6
        //qrAlgoSteps.add(new QRAlgoCornerFilter(qrAlgoSteps.get(5), qrAlgoSteps.get(4), 3, 48));//7
        qrAlgoSteps.add(new QRAlgoSquareFilter(9, qrAlgoSteps.get(5), qrAlgoSteps.get(4), qrAlgoSteps.get(5), 60, 5)); //6
        qrAlgoSteps.add(new QRAlgoPolygonFilter(qrAlgoSteps.get(6))); //7
        qrAlgoSteps.add(new QRAlgoAreaFilter(qrAlgoSteps.get(7)));
        qrAlgoSteps.add(new QRAlgoTraceImage(qrAlgoSteps.get(0), qrAlgoSteps.get(8)));
        return qrAlgoSteps;
    }

}
