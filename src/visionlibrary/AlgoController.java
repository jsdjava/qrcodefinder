package visionlibrary;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;

/**
 *
 * @author daviesj
 */
public abstract class AlgoController {

    protected AlgoView view;
    protected int curAlgo = -1;
    protected List<QRAlgoStep> qrAlgoSteps = new ArrayList();
    protected QRCodeData curData;
    protected JFrame frm;
    protected WebcamReader reader;

    public AlgoController(String frmTitle, AlgoView view, boolean enableCamera) {
        this.view = view;
        frm = new JFrame(frmTitle);
        Dimension curResolution = Toolkit.getDefaultToolkit().getScreenSize();
        frm.setSize(new Dimension(curResolution.width, curResolution.height - 50));
        this.view = view;
        frm.add(view);
        frm.setVisible(true);
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        if (enableCamera) {
            frm.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                    if (reader != null) {
                        reader.closeWebcam();
                    }
                }
            });
            //Webcam Stuff
            Thread t = new Thread() {
                public void run() {
                    reader = new WebcamReader();
                    while (frm.isVisible()) {
                        try {
                            view.setCameraImg(reader.getImage());
                            Thread.sleep(100);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            };
            t.start();
        } else {
            view.disableCamera();
        }
        view.setController(this);
    }

    protected abstract List<QRAlgoStep> constructAlgorithmSequence(QRAlgoStep loadStep);

    /**
     * This processes an image from the camera using the construct algorithm
     * sequence with the current camera image passed in. It then determines the time
     * it took to process that image.
     */
    public void processCameraImg() {
        long startTime = System.currentTimeMillis();
        BufferedImage curCamImg = view.getCameraImg();
        if (curCamImg != null) {
            view.setProcessImgMode(true);
            List<QRAlgoStep> qrCameraSteps = constructAlgorithmSequence(new QRAlgoWrapImages(curCamImg));
            for (QRAlgoStep curStep : qrCameraSteps) {
                curStep.completeStep();
            }
            qrCameraSteps.get(qrCameraSteps.size() - 1).getData().display(view.getCameraGraphics(), view.getCameraWidth(), view.getCameraHeight());
        }
        long endTime = System.currentTimeMillis();
        System.out.println("Processed the camera image in " + (endTime - startTime) + " ms");
    }

    /**
     * Proceeds to the next algorithm if there are more algorithms left to do.
     */
    public void nextAlgo() {
        if (curAlgo < qrAlgoSteps.size() - 1) {
            curAlgo++;
        }
        doAlgo();
    }

    /**
     * Moves back to the prior algorithm, unless this is the first algorithm.
     */
    public void prevAlgo() {
        if (curAlgo > 0) {
            curAlgo--;
        }
        doAlgo();
    }

    /**
     * This calls the complete step of the current algorithm, only if it hasn't
     * already been completed.It then updates the view based on the results.
     */
    private void doAlgo() {
        if (qrAlgoSteps != null && (!qrAlgoSteps.isEmpty())) {
            if (qrAlgoSteps.get(curAlgo).getData() == null) {
                qrAlgoSteps.get(curAlgo).completeStep();
            }
            int oldImgSpot = 0;
            if (curData != null) {
                oldImgSpot = curData.getImageSpot();
            }
            curData = qrAlgoSteps.get(curAlgo).getData();
            curData.setImageSpot(oldImgSpot);
            updateView();
        }
    }

    /**
     * Does every single algorithm until there aren't any left.
     */
    public void allAlgos() {
        while (curAlgo < qrAlgoSteps.size() - 1) {
            nextAlgo();
        }
    }

    /**
     * This advances to the next image in the current list of images.
     */
    public void nextImg() {
        if (curData != null) {
            curData.nextImage();
        }
        view.resetZoom();
        updateView();
    }

    /**
     * This shows the previous image in the current list of images.
     */
    public void prevImg() {
        if (curData != null) {
            curData.prevImage();
        }
        view.resetZoom();
        updateView();
    }

    /**
     * This updates the view inside the QRCodePanel to show the result of the current algorithm.
     */
    private void updateView() {
        if (curData != null) {
            curData.display(view.getCanvasGraphics(), view.getGraphicsWidth(), view.getGraphicsHeight());
            view.setAlgoName(qrAlgoSteps.get(curAlgo).getName());
            view.repaint();
        }
    }

}
