/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visionlibrary;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

/**
 *
 * @author daviesj
 */
public abstract class AlgoView extends JPanel {

    public abstract void resetZoom();

    public abstract void setAlgoName(String s);

    public abstract void setCameraImg(BufferedImage img);

    public abstract BufferedImage getCameraImg();

    public abstract Graphics getCameraGraphics();

    public abstract int getCameraWidth();

    public abstract int getCameraHeight();

    public abstract Graphics getCanvasGraphics();

    public abstract int getGraphicsWidth();

    public abstract int getGraphicsHeight();

    ;
    public abstract void setController(AlgoController controller);

    public abstract void setProcessImgMode(boolean b);

    public abstract void disableCamera();
}
