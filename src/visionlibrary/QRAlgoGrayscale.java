package visionlibrary;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import visionlibrary.QRAlgoStep;
import visionlibrary.QRCodeData;

/**
 * This returns an image that has been gray scaled in order to make it color
 * invariant.
 *
 * @author daviesj
 */
public class QRAlgoGrayscale extends QRAlgoStep {

    private QRAlgoStep prevStep;

    /**
     * This mostly just requires that a colored image be passed in.
     *
     * @param prevStep The last algorithm, probably just one where the images were laoded.
     */
    public QRAlgoGrayscale(QRAlgoStep prevStep) {
        this.prevStep = prevStep;
    }

    /**
     * Returns a grayscale version of the passsed in images, where it computes the value at each pixel
     * as the (baseRed + baseBlue+ baseGreen)/3.
     *
     * @return A grayscale version of the passed in images.
     */
    @Override
    public QRCodeData completeStep() {
        QRCodeData greyedImages = new QRCodeData(new ArrayList());
        QRCodeData qrData = prevStep.getData();
        if (qrData != null) {
            for (BufferedImage baseImg : qrData.getImages()) {
                BufferedImage greyedImg = new BufferedImage(baseImg.getWidth(), baseImg.getHeight(), BufferedImage.TYPE_INT_RGB);
                for (int i = 0; i < baseImg.getWidth(); i++) {
                    for (int j = 0; j < baseImg.getHeight(); j++) {
                        Color baseColor = new Color(baseImg.getRGB(i, j));
                        int grayValue = (baseColor.getRed() + baseColor.getGreen() + baseColor.getBlue()) / 3;
                        greyedImg.setRGB(i, j, new Color(grayValue, grayValue, grayValue).getRGB());
                    }
                }
                greyedImages.addImage(greyedImg);
            }
        }
        result = greyedImages;
        return greyedImages;
    }
}
