package visionlibrary;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;

/**
 * This loads a bunch of images using a specified directory, and a list of allowed
 * image extensions.
 *
 * @author daviesj
 */
public class QRAlgoLoadImages extends QRAlgoStep {

    private String startDir;
    private String[] imgExtensions = {".png", ".jpg"};

    /**
     * This just requires the address of the start directory to look for images in.
     *
     * @param startDir The start directory to search for images in.
     */
    public QRAlgoLoadImages(String startDir) {
        this.startDir = startDir;
    }

    /**
     * This loads all the images that it can find inside a directory into a list of the images.
     * @return A list of loaded images.
     */
    @Override
    public QRCodeData completeStep() {
        QRCodeData allImgData = new QRCodeData(new ArrayList());
        File startDirectory = new File(startDir);
        if (startDirectory.isDirectory()) {
            for (File curFile : startDirectory.listFiles()) {
                boolean isImgFile = false;
                for (String curImgExtension : imgExtensions) {
                    if (curFile.getName().contains(curImgExtension)) {
                        isImgFile = true;
                        break;
                    }
                }
                if (isImgFile) {
                    try {
                        allImgData.addImage(ImageIO.read(curFile));
                    } catch (IOException e) {
                        JOptionPane.showMessageDialog(null, "Okay, we couldn't read the file for an unknown reason");
                    }
                }
            }
        } else {
            JOptionPane.showMessageDialog(null, "Well, thats not a directory");
        }
        result = allImgData;
        return allImgData;
    }

}
