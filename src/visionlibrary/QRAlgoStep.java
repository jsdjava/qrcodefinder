package visionlibrary;

/**
 * This is an abstract class that should be overriden by any algorithm
 * It has a method of storing a list of images, and of doing a computation on all of them,
 * and finally returning the result as a new data object. I should have probably written the image
 * iterator inside complete step inside this, since there is absolutely no point to rewriting it in
 * every algorithm, but oh well.
 *
 * @author daviesj
 */
public abstract class QRAlgoStep {

    /**
     * Set this in order to make sure that the return value of get data is not null but the value of the computation.
     */
    protected QRCodeData result;

    /**
     * This should be overriden, it should set result at the end, and it basically does a computation specified in the child class
     * on the QRCodeData objects.
     *
     * @return A data object that holds a list of usually images, or sometimes polygons.
     */
    public abstract QRCodeData completeStep();

    /**
     * This returns the result of completeStep, or null if completeStep hasn't been done
     * yet.
     *
     * @return A data object that holds a list of images, or sometimes polygons.
     */
    public QRCodeData getData() {
        return result;
    }

    /**
     * Returns the name of the current algorithm, which by default just uses the class name.
     *
     * @return A string that represents the name of the current algorithm step.
     */
    public String getName() {
        return getClass().getSimpleName();
    }

}
