package visionlibrary;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import visionlibrary.QRAlgoStep;
import visionlibrary.QRCodeData;

/**
 * This wraps an image, or list of images, from somewhere else, like a Webcam, into
 * a QRCodeData object.
 *
 * @author daviesj
 */
public class QRAlgoWrapImages extends QRAlgoStep {

    private List<BufferedImage> images;

    /**
     * Pass in an image into this to wrap a single image into a qr code data
     * object.
     *
     * @param img The image to wrap into a QRCodeData
     */
    public QRAlgoWrapImages(BufferedImage img) {
        images = new ArrayList();
        images.add(img);
    }

    /**
     * Wraps a list of images into a QRCodeData object.
     *
     * @param images The list of images.
     */
    public QRAlgoWrapImages(List<BufferedImage> images) {
        this.images = images;
    }

    /**
     * This loads all the images that it can find inside a directory into a list of the images.
     *
     * @return A list of loaded images.
     */
    @Override
    public QRCodeData completeStep() {
        QRCodeData allImgData = new QRCodeData(new ArrayList());
        for (BufferedImage curImg : images) {
            allImgData.addImage(curImg);
        }
        result = allImgData;
        return allImgData;
    }
}
