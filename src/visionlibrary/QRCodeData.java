package visionlibrary;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

/**
 * This class just represented an easy way to pass data between each of the QRAlgoSteps.
 * In retrospect, I probably should have used some kind of pattern here to simplify this setup,
 * since this is just an image wrapper, and the QRPolygonData extension of this is really clunky.
 * Currently, all this does is store a list of images, and can flip between each image and display it.
 *
 * @author daviesj
 */
public class QRCodeData {

    private List<BufferedImage> images;
    protected int imgItr;

    /**
     * Creates a new QRCodeData object with an empty List of images.
     */
    public QRCodeData() {
        images = new ArrayList();
    }

    /**
     * Same as the default constructor, except it sets the inner list to the passed in one.
     *
     * @param images The list of images to set the list of images inside here to.
     */
    public QRCodeData(List<BufferedImage> images) {
        this.images = images;
        imgItr = 0;
    }

    /**
     * This draws the current image to a graphics context.
     *
     * @param q      The graphics object to draw to.
     * @param width  The width of the canvas, currently not used
     * @param height The height of the canvas, also not used
     */
    public void display(Graphics q, int width, int height) {
        if (imgItr >= 0 && imgItr < images.size()) {
            BufferedImage curImg = images.get(imgItr);
            //((Graphics2D) q).scale(((double) width) / ((double) curImg.getWidth()), ((double) height) / ((double) curImg.getHeight()));
            q.drawImage(curImg, 0, 0, null);
        }
    }

    /**
     * Adds an image to the list of images.
     *
     * @param newImg The new image to add.
     */
    public void addImage(BufferedImage newImg) {
        images.add(newImg);
    }

    /**
     * Returns the current list of images. This should probably be wrapped or something
     * since this could just be cleared.
     *
     * @return The list of images stored in here.
     */
    public List<BufferedImage> getImages() {
        return images;
    }

    /**
     * Advances the current image to the next image in the list, if its not already at
     * the final image in the list.
     */
    public void nextImage() {
        if (imgItr < images.size() - 1) {
            imgItr++;
        }
    }

    /**
     * Moves to the previous image in the list, unless its already at the 0th image index.
     */
    public void prevImage() {
        if (imgItr > 0) {
            imgItr--;
        }
    }

    /**
     * Sets the current image spot to 0th image index. I don't think this is used either.
     */
    public void zeroImage() {
        imgItr = 0;
    }

    /**
     * Sets the image spot to the new specified one. I don't think this is used.
     *
     * @param spot The spot to set the current image index to.
     */
    public void setImageSpot(int spot) {
        if (spot >= 0 && spot < images.size()) {
            imgItr = spot;
        } else {
            imgItr = 0;
        }
    }

    /**
     * Returns the current image index
     *
     * @return The index of the current image
     */
    public int getImageSpot() {
        return imgItr;
    }
}
