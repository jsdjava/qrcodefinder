package visionlibrary;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.List;

/**
 * This class holds a collection of points. It is useful for holding pixels
 * that are marked as being members of a square. It also contains a lot
 * of code for calculating slopes and angles.
 *
 * @author daviesj
 */
public class QRPolygon {

    private Point[] points;
    private List<Point>[] pixels;

    public QRPolygon(Point[] points) {
        this.points = points;
    }

    /**
     * Requires an array of points that represent the actual polygon. Note that they must
     * be in either clockwise or counterclockwise order.
     *
     * @param points The order array of points.
     * @param pixels A list of all the pixels on each edge of the shape that can be used
     *               to calculate a more accurate slope at each one using linear regression
     */
    public QRPolygon(List<Point>[] pixels, Point[] points) {
        this.points = points;
        this.pixels = pixels;
    }

    /**
     * Returns a bounding box for the polygon that surrounds it entirely.
     *
     * @return A bounding box for this shape.
     */
    public Rectangle getRectangle() {
        int minX = points[0].x;
        int minY = points[0].y;
        int maxX = points[0].x;
        int maxY = points[0].y;
        for (Point curPoint : points) {
            if (curPoint.x < minX) {
                minX = curPoint.x;
            }
            if (curPoint.x > maxX) {
                maxX = curPoint.x;
            }
            if (curPoint.y < minY) {
                minY = curPoint.y;
            }
            if (curPoint.y > maxY) {
                maxY = curPoint.y;
            }
        }
        return new Rectangle(minX, minY, maxX - minX, maxY - minY);
    }

    /**
     * Gets the intersections of two lines each defined by two
     * different points.
     *
     * @param l1p1 A point on the first line
     * @param l1p2 Another point on the first line
     * @param l2p1 A point on the second line
     * @param l2p2 Another point on the second line
     * @return The intersection of the two points, or (0,0) if the lines are parallel.
     */
    public Point getLineIntersection(Point l1p1, Point l1p2, Point l2p1, Point l2p2) {
        double firstLineSlope = ((double) l1p1.y - l1p2.y) / ((double) l1p1.x - l1p2.x);
        double secondLineSlope = ((double) l2p1.y - l2p2.y) / ((double) l2p1.x - l2p2.x);
        return getLineIntersection(firstLineSlope, l1p1, secondLineSlope, l2p1);
    }

    /**
     * Gets the intersection of two lines each defined by a slope and a point.
     *
     * @param firstLineSlope  The slope of the first line
     * @param firstLinePoint  A point on the first line
     * @param secondLineSlope The slope of the second line
     * @param secondLinePoint A point on the second line
     * @return A point where the two lines intersect, or (0,0) if they are parallel.
     */
    public Point getLineIntersection(double firstLineSlope, Point firstLinePoint, double secondLineSlope, Point secondLinePoint) {
        double firstAngle = Math.atan(firstLineSlope);
        double secondAngle = Math.atan(secondLineSlope);
        double firstLineInt = -firstLineSlope * firstLinePoint.x + firstLinePoint.y;
        double secondLineInt = -secondLineSlope * secondLinePoint.x + secondLinePoint.y;
        if (firstAngle == secondAngle) {
            System.out.println("They are parallel!");
            return new Point(0, 0);
        } else if (Math.abs(Math.toDegrees(firstAngle)) > 89.0f) { //Use the equation of the second line, plug in the y 
            return new Point(firstLinePoint.x, (int) (secondLineSlope * firstLinePoint.x + secondLineInt));

        } else if (Math.abs(Math.toDegrees(secondAngle)) > 89.0f) {
            return new Point(secondLinePoint.x, (int) (firstLineSlope * secondLinePoint.x + firstLineInt));
        } else {
            Point result = new Point();
            result.x = (int) ((secondLineInt - firstLineInt) / (firstLineSlope - secondLineSlope));
            result.y = ((int) (firstLineSlope * ((double) result.x) + firstLineInt));
            return result;
        }
    }

    /**
     * Returns 1/2 the average size of the sides inside this polygon
     *
     * @return The average size of the polygon sides divided by 2
     */
    public double getSize() {
        double avgSize = 0;
        if (points.length == 4) {
            Point oldPoint = null;
            for (Point curPoint : points) {
                if (oldPoint != null) {
                    int xDelta = oldPoint.x - curPoint.x;
                    int yDelta = oldPoint.y - curPoint.y;
                    avgSize += Math.sqrt(xDelta * xDelta + yDelta * yDelta);
                    oldPoint = curPoint;
                }
            }
            avgSize += Math.sqrt((points[0].x - points[3].x) * (points[0].x - points[3].x) + (points[0].y - points[3].y) * (points[0].y - points[3].y));
        }
        return avgSize / 8;
    }

    /**
     * Returns a new polygon with a list of points in each corner poly passed in
     * that is furthest from this polygon's centerpoint.
     *
     * @param cornerPolys The list of polygons to find the farthest point from this polygons centerpoint
     * @return A new polygon that encloses each of these points.
     */
    public QRPolygon grow(QRPolygon[] cornerPolys) {
        Point[] newPoints = new Point[3];
        Point centerPoint = getQuadriCenterPoint();
        for (int i = 0; i < 3; i++) {
            newPoints[i] = cornerPolys[i].getFarthestPoint(centerPoint);
        }
        return new QRPolygon(null, newPoints);
    }

    /**
     * This finds the point inside this polygon that is farthest from the
     * passed in point.
     *
     * @param checkPoint The point to find the max distance from
     * @return The point in this polygon that was farthest from the checkPoint
     */
    public Point getFarthestPoint(Point checkPoint) {
        Point farthestPoint = points[0];
        int maxDist = 0;
        for (Point curPoint : points) {
            int xDelta = curPoint.x - checkPoint.x;
            int yDelta = curPoint.y - checkPoint.y;
            if (Math.sqrt(xDelta * xDelta + yDelta * yDelta) > maxDist) {
                maxDist = (int) Math.sqrt(xDelta * xDelta + yDelta * yDelta);
                farthestPoint = new Point(curPoint);
            }
        }
        return farthestPoint;
    }

    /**
     * This only works on 4 point polygon (quadrilateral)
     * But if basically finds the center of the polygon based on the intersection
     * of the two diagonals.
     *
     * @return The centerpoint of the polygon
     */
    public Point getQuadriCenterPoint() {
        Point centerPoint = new Point();
        if (points.length == 4) {
            return getLineIntersection(points[0], points[2], points[1], points[3]);
        }
        return centerPoint;
    }

    /**
     * If there is a passed in list of pixels,this gets called by the slope
     * methods in order to compute a more accurate slope at each line. It uses the
     * linear regression method to determine what the slope of the line is based on
     * each pixel in the list passed in, and if its R value is greater than 0.75f,
     * it allows it through.
     *
     * @param pixels The pixels to input into the linear regression formula.
     * @return The slope of the line of best fit going through the pixels, or Double.NaN if it has low accuracy.
     */
    private double getLinearRegression(List<Point> pixels) {
        long size = pixels.size();
        double sumX = 0, sumY = 0, sumXY = 0, sumXSquared = 0, sumYSquared = 0;
        for (Point curPixel : pixels) {
            sumX += curPixel.x;
            sumY += curPixel.y;
            sumXY += curPixel.x * curPixel.y;
            sumXSquared += curPixel.x * curPixel.x;
            sumYSquared += curPixel.y * curPixel.y;
        }
        double slopeTop = size * sumXY - sumX * sumY;
        double slopeBot = size * sumXSquared - sumX * sumX;
        //if (slopeTop == -966.0 && slopeBot == 115.0) {
        //    for (Point curPoint : pixels) {
        //        System.out.println(curPoint);
        //    }
        //    System.out.println(sumX);
        //    System.out.println(sumY);
        //    System.out.println(sumXY);
        //    System.out.println(sumXSquared);
        //}
        double coeffOfDet = slopeTop / ((Math.sqrt(slopeBot)) * (Math.sqrt(size * sumYSquared - sumY * sumY)));
        //System.out.println("slopeTop " + slopeTop);
        //System.out.println("slopeBot " + slopeBot);
        //System.out.println("slopeTop/slopeBot " + (slopeTop / slopeBot));
        if (Math.abs(coeffOfDet) < 0.75f || (!Double.isFinite(coeffOfDet))) {
            return Double.NaN;
        }
        return slopeTop / slopeBot;
    }

    private static final double MAX_ANGLE_DIFF = 150.0f;

    /**
     * This is the sort of fuzzy formula that determines what the smallest difference
     * between the passed in angle and the angles inside this shape. It then returns
     * that difference in degrees, and the associated slope. It also has a check for
     * differences that are greater than 150 degrees that rules them as being in the wrong
     * direction and recomputes them.
     *
     * @param triAngle The angle to compare against each one.
     * @return the difference in degrees, and the slope at that angle.
     */
    public double[] calculateMinDiff(double triAngle) {
        double firstAngle = Math.toDegrees(Math.atan(((double) points[0].y - points[3].y) / ((double) (points[0].x - points[3].x))));
        double angleDiff = Math.abs(firstAngle - triAngle);
        if (angleDiff > MAX_ANGLE_DIFF) { //Theres a really annoying case where triangles that have sides sloped slightly in the wrong direction mark the wrong center. This is a sanity check sort of thing.
            angleDiff = 180.0f - angleDiff;
        }
        double[] answer = new double[]{angleDiff, getOtherSquareSlope()};
        if (points.length == 4) {
            for (int i = 0; i < points.length - 1; i++) {
                double checkAngle = (Math.toDegrees(Math.atan(((double) points[i].y - points[i + 1].y) / ((double) (points[i].x - points[i + 1].x)))));
                angleDiff = Math.abs(checkAngle - triAngle);
                if (angleDiff > MAX_ANGLE_DIFF) { //Theres a really annoying case where triangles that have sides sloped slightly in the wrong direction mark the wrong center. This is a sanity check sort of thing.
                    angleDiff = 180.0f - angleDiff;
                }
                if (angleDiff < answer[0]) {
                    answer[0] = angleDiff;
                    if (i % 2 == 0) {
                        answer[1] = getFirstSquareSlope();
                    } else {
                        answer[1] = getOtherSquareSlope();
                    }
                }
            }
        }
        return answer;
    }

    /**
     * Returns one of the slopes in the square based on either a regression result if
     * it can, or just the difference between the two points if the regression didn't
     * work. It used to average with the slope of the opposite side, but this would sometimes
     * cause slopes to cancel each other, so it no longer does this.
     *
     * @return
     */
    public double getFirstSquareSlope() {
        double firstSlope = ((double) points[0].y - points[1].y) / ((double) points[0].x - points[1].x);
        if (pixels != null) {
            double regressionAnswer = getLinearRegression(pixels[1]);
            if (Double.isFinite(regressionAnswer)) {
                //System.out.println("slope " + firstSlope);
                //System.out.println("regression " + (regressionAnswer / 2.0f));
                return regressionAnswer;
            }
        }
        double secondSlope = ((double) points[3].y - points[2].y) / ((double) (points[3].x - points[2].x));
        return firstSlope;
    }

    /**
     * Returns the other slope in the square based on either a regression result if
     * it can, or just the difference between the two points if the regression didn't
     * work. It used to average with the slope of the opposite side, but this would sometimes
     * cause slopes to cancel each other, so it no longer does this.
     *
     * @return
     */
    public double getOtherSquareSlope() {
        double firstSlope = ((double) points[1].y - points[2].y) / ((double) points[1].x - points[2].x);
        if (pixels != null) {
            double regressionAnswer = getLinearRegression(pixels[2]);
            if (Double.isFinite(regressionAnswer)) {
                //System.out.println("slope " + firstSlope);
                //System.out.println("regression " + (regressionAnswer / 2.0f));
                return regressionAnswer;
            }
        }
        double secondSlope = ((double) points[0].y - points[3].y) / ((double) (points[0].x - points[3].x));
        return firstSlope;
    }

    /**
     * This returns a list of the three slopes of the lines inside this triangle
     * as compared to a line at 0 degrees. It will return null if the polygon has
     * less than 3 or more than 3 points.
     *
     * @return The three angles of the triangle, in degrees.
     */
    public double[] getTriangleAngles() {
        double[] angles = new double[3];
        if (points.length == 3) {
            for (int i = 0; i < points.length - 1; i++) {
                angles[i] = (Math.toDegrees(Math.atan(((double) points[i].y - points[i + 1].y) / ((double) (points[i].x - points[i + 1].x)))));
            }
            angles[2] = (Math.toDegrees(Math.atan(((double) points[0].y - points[2].y) / ((double) (points[0].x - points[2].x)))));
            return angles;
        }
        return null;
    }

    /**
     * Draws the polygon at the correct coordinates,using the magenta color.
     *
     * @param q The graphics context to draw to
     */
    public void display(Graphics q) {
        q.setColor(Color.MAGENTA);
        for (int curPoint = 0; curPoint < points.length - 1; curPoint++) {
            q.drawLine(points[curPoint].x, points[curPoint].y, points[curPoint + 1].x, points[curPoint + 1].y);
        }
        q.drawLine(points[0].x, points[0].y, points[points.length - 1].x, points[points.length - 1].y);
        Color[] colors = new Color[]{Color.GREEN, Color.YELLOW, Color.RED, Color.WHITE};
        if (pixels != null) {
            Point oldPoint = points[3];
            for (int i = 0; i < pixels.length; i++) {
                q.setColor(colors[i]);
                for (Point curPoint : pixels[i]) {
                    q.drawLine(curPoint.x, curPoint.y, curPoint.x, curPoint.y);
                }
                Point curPoint = points[i];
                q.fillRect(oldPoint.x + (curPoint.x - oldPoint.x) / 2, oldPoint.y + (curPoint.y - oldPoint.y) / 2, 10, 10);
                oldPoint = curPoint;
            }
        }
        Rectangle boundsRect = getRectangle();
        //q.drawRect(boundsRect.x, boundsRect.y, boundsRect.width, boundsRect.height);
    }

}
