package visionlibrary;

import com.github.sarxos.webcam.Webcam;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * This is a really simple class that I wrote using a library that I found online
 * It makes using the Webcam extremely easy because its like three lines to go from
 * opening a webcam to getting images from it.
 *
 * @author daviesj
 */
public class WebcamReader {

    private static BufferedImage curImg = null;

    public static void main(String[] args) {
        Webcam webcam = Webcam.getDefault();
        //webcam.setCustomViewSizes(new Dimension[]{WebcamResolution.HD720.getSize()});
        //webcam.setViewSize(WebcamResolution.HD720.getSize());
        webcam.setViewSize(new Dimension(640, 480));
        webcam.open();
        curImg = webcam.getImage();
        JFrame frm = new JFrame("Test");
        frm.setSize(1000, 1000);
        JPanel pan = new JPanel() {
            @Override
            public void paintComponent(Graphics q) {
                q.drawImage(curImg, 0, 0, null);
            }
        };
        frm.add(pan);
        frm.setVisible(true);
        while (true) {
            try {
                curImg = webcam.getImage();
                frm.repaint();
                Thread.sleep(10);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private Webcam webcam;
    private static final int WIDTH = 640;
    private static final int HEIGHT = 480;

    /**
     * This returns a new webcam reader object, which can be polled for images.
     * It attempts to open a webcam that is 640x480, and if it fails it displays
     * a JOptionPane error.
     */
    public WebcamReader() {
        try {
            webcam = Webcam.getDefault();
            //webcam.setCustomViewSizes(new Dimension[]{WebcamResolution.HD720.getSize()});
            //webcam.setViewSize(WebcamResolution.HD720.getSize());
            webcam.setViewSize(new Dimension(WIDTH, HEIGHT));
            webcam.open();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Sorry, can't load a webcam for some reason");
        }
    }

    /**
     * Returns the current image from the webcam, or if its null it returns
     * an empty image with the same dimensions.
     *
     * @return
     */
    public BufferedImage getImage() {
        if (webcam == null) {
            return new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
        }
        return webcam.getImage();
    }

    /**
     * Shuts down the webcam as long as it isn't null.
     */
    public void closeWebcam() {
        if (webcam != null) {
            webcam.close();
        }
    }

}
